# 信息系统分析与设计

![教材](./images/book.jpg)

## 实验背景-生产管理系统

- 系统背景
生产管理系统背景：某企业的业务是生产并销售一类机电产品。基本流程是接收客户订单，从供应商处采购原材料，生产加工为成品，然后将成品发货给客户。

- 业务流程
企业业务活动中有不少流程，比如订客户订单流程，采购流程，生产流程，出入库流程，发货流程，售后维修流程等。

- 数据
企业业务活动中有不少数据，如：员工，客户，供应商，物料，订单，物流单等。特别注意，物料数据采用BOM方式以树状结构存储。

- 数据分析与统计
企业需要一些数据分析与统计工作，如：MRP，排程排产，最低库存采购，成本及利润分析，导出各类统计报表等。

## 实验准备

- 每个同学必须有一个gitlab账号，并上传SSH公钥。
- 每4到5人一个组，每个组一个组长，由组长创建一个私有项目，项目名称必须是is_experiment
- 由组长将组员加入到项目中，成员的角色是developer
- 由组长将老师也加入到项目中。老师的账号是zwdcdu，将老师的角色也设置为developer。
- 在项目中创建5个文件夹：test1,test2,test3,test4,test5。表示5次实验。每次实验放在各自的文件夹中。
- 每个目录中必须有一个文件README.md。注意大写小写。
- 在is_experiment根目录中README.md文件的内容是班级，用表格的形式写上项目成名的学号，姓名，介绍等信息。
- 在5个test目录中的README.md文件是主要的实验报告，说明每个实验的主要内容以及完成人员。除了README.md文件外，还必须有其他辅助文件。README.md文件通过链接，引用其他文件。
- 实验环境：文档编写工具用vscode或者其他支持git的工具。
- 实验环境：markdown,plantuml,mermaid等工具和标准书写文字和绘制图形，尽量不要使用word,excel,photoshop等工具。
- 每次实验中每个项目成员都必须有单独的提交痕迹，不能只是由一个同学提交，以区分团队成员的分工和工作量。原则上团队成员只维护自己的文件。
- 由于老师也是你项目中的成员，所以老师能看见你的项目并批阅，评估每个同学的工作量。
- 本项目是私有项目，因此同班的其他团队的人员是看不见的。
- 如果vscode中不能预览图像，要安装graphviz，下载[graphviz-2.38.msi](./graphviz-2.38.msi)。
- 在windows系统中安装git环境工具。下载[gitgfb_ttrar.rar](./gitgfb_ttrar.rar)

## 重要

- 每次提交时，必须写上姓名和提交原因，如“张三做了某工作”。让老师知道是你做的。计算你的工作量。

## 实验1：生产管理系统流程分析

- 实验目的
  - 分析生产管理系统中的部分流程，以文字/数据流图/业务流程图的绘制出一些简单的业务流程。
  - 学习通过git团队开发的原理和方法。
- 最后提交日期:2022-1-18

## 参考资料

- https://gitee.com/zwdcdu/is_analysis
- 绘制方法参考：[PlantUML标准](http://plantuml.com)
- [PlantUML GitHub 官方网站](https://github.com/plantuml/plantuml)
- PlantUML服务: http://plantuml.com/zh/server
- plantuml在线编辑器： http://www.plantuml.com/plantuml
- Markdown格式参考： https://www.jianshu.com/p/b03a8d7b1719
- Git简书 https://git-scm.com/book/zh/v2
- Git分支 https://git-scm.com/book/zh/v1/Git-%E5%88%86%E6%94%AF
- Github 简明教程-操作标签 https://www.cnblogs.com/tracylxy/p/6439089.html
- Git菜鸟教程 http://www.runoob.com/git/git-tutorial.html
- [GitWindows客户端](./gitgfb_ttrar.rar)
- 版本控制样例参见：https://github.com/oracle/db-sample-schemas
- 文档编写工具 Sphinx 使用手册 https://of.gugud.com/t/topic/185 https://zh-sphinx-doc.readthedocs.io/en/latest/contents.html
- [GitHub开发者接口V3](https://developer.github.com/v3/)
- [mermaid](https://gitee.com/mirrors/mermaid/blob/develop/README.zh-CN.md)
- [gitlab](https://gitlab.com/users/sign_in?redirect_to_referer=yes)
- [GitLab 之 PlantUML 的配置及使用](https://blog.csdn.net/aixiaoyang168/article/details/76888254)
- [gitlab markdown](https://docs.gitlab.com/ee/user/markdown.html)
- 创建github账号，用github账号登录gitlab
- [git 多账号 ssh-key 管理（github和gitlab共同使用）](https://blog.csdn.net/qq_30227429/article/details/80229167)
